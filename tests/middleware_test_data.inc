<?php

/**
 * @file
 * Data for running simpletests
 */

// Misc data required by tests

        $content['type']  = array(
            'name'        => 'TestType',
            'type'        => 'testtype',
            'description' => 'Test content type',
            'title_label' => 'Title',
            'body_label'  => 'Body',
            'min_word_count' => '0',
            'help' => '',
            'node_options' => array('status' => TRUE, 'promote' => TRUE, 'sticky' => FALSE, 'revision' => FALSE),
            'language_content_type' => '2',
            'upload' => 1,
            'i18n_node' => '1',
            'i18nsync_nodeapi' => array(
            'author' => FALSE, 'status' => FALSE, 'promote' => FALSE, 'moderate' => FALSE, 'sticky' => FALSE, 'revision' => FALSE, 'parent' => FALSE, 'taxonomy' => FALSE, 'files' => FALSE, 'field_logo' => FALSE),
            'old_type' => 'TestType',
            'orig_type' => '',
            'module' => 'node',
            'custom' => '1',
            'modified' => '1',
            'locked' => '0',
            'content_profile_use' => 0,
        );
        $content['fields']  = array(
            0 => array(
                'label' => 'Test Image field',
                'field_name' => 'field_image',
                'type' => 'filefield',
                'widget_type' => 'imagefield_widget',
                'change' => 'Change basic information',
                'weight' => '31',
                'file_extensions' => 'jpg gif png',
                'progress_indicator' => 'bar',
                'file_path' => 'test',
                'max_filesize_per_file' => '',
                'max_filesize_per_node' => '',
                'max_resolution' => 0,
                'min_resolution' => 0,
                'custom_alt' => 0,
                'alt' => '',
                'custom_title' => 0,
                'title_type' => 'textfield',
                'title' => '',
                'use_default_image' => 0,
                'default_image_upload' => '',
                'default_image' => NULL,
                'description' => '',
                'group' => FALSE,
                'required' => 0,
                'multiple' => '0',
                'list_field' => '0',
                'list_default' => 1,
                'description_field' => '0',
                'op' => 'Save field settings',
                'module' => 'filefield',
                'widget_module' => 'imagefield',
                'columns' => array(
                    'fid' => array('type' => 'int', 'not null' => FALSE, 'views' => TRUE),
                    'list' => array('type' => 'int', 'size' => 'tiny', 'not null' => FALSE, 'views' => TRUE),
                    'data' => array('type' => 'text', 'serialize' => TRUE, 'views' => TRUE),
                ),
                'display_settings' => array(
                    'label'  => array('format' => 'above', 'exclude' => 0),
                    'teaser' => array('format' => 'image_plain', 'exclude' => 0),
                    'full'   => array('format' => 'image_plain', 'exclude' => 0),
                    4 => array('format' => 'image_plain', 'exclude' => 0),
                ),
            ),
            1 => 
            array(
              'label'         => 'Test text field',
              'field_name'    => 'field_textfield',
              'type'          => 'text',
              'widget_type'   => 'text_textfield',
              'change'        => 'Change basic information',
              'weight'        => '32',
              'rows'          => 5,
              'size'          => '60',
              'description'   => '',
              'default_value' => array( 0 => array('value' => '', '_error_element' => 'default_value_widget][field_contact][0][value')),
              'default_value_php'    => '',
              'default_value_widget' => array('field_contact' => array(0 => array('value' => '', '_error_element' => 'default_value_widget][field_contact][0][value'))),
              'group'         => FALSE,
              'required'      => 0,
              'multiple'      => '0',
              'text_processing' => '0',
              'max_length'    => '',
              'allowed_values' => '',
              'allowed_values_php' => '',
              'op' => 'Save field settings',
              'module' => 'text',
              'widget_module' => 'text',
              'columns' => 
              array(
                'value' => 
                array(
                  'type' => 'text',
                  'size' => 'big',
                  'not null' => FALSE,
                  'sortable' => TRUE,
                  'views' => TRUE,
                ),
              ),
              'display_settings' => 
              array(
                'label' => 
                array(
                  'format' => 'above',
                  'exclude' => 0,
                ),
                'teaser' => 
                array(
                  'format' => 'default',
                  'exclude' => 0,
                ),
                'full' => 
                array(
                  'format' => 'default',
                  'exclude' => 0,
                ),
                4 => 
                array(
                  'format' => 'default',
                  'exclude' => 0,
                ),
              ),
            ),
            2 => 
            array(
              'label' => 'Test taxonomy field',
              'field_name' => 'field_taxonomy_test',
              'type' => 'content_taxonomy',
              'widget_type' => 'content_taxonomy_options',
              'change' => 'Change basic information',
              'weight' => '1',
              'show_depth' => 1,
              'group_parent' => '0',
              'description' => '',
              'default_value' => 
              array(
                0 => 
                array(
                  'value' => NULL,
                ),
              ),
              'default_value_php' => '',
              'default_value_widget' => NULL,
              'group' => FALSE,
              'required' => 0,
              'multiple' => '2',
              'save_term_node' => 0,
              'vid' => '10',
              'parent' => '0',
              'parent_php_code' => '',
              'depth' => '',
              'op' => 'Save field settings',
              'module' => 'content_taxonomy',
              'widget_module' => 'content_taxonomy_options',
              'columns' => 
              array(
                'value' => 
                array(
                  'type' => 'int',
                  'not null' => FALSE,
                  'sortable' => FALSE,
                ),
              ),
              'display_settings' => 
              array(
                'label'  => array('format' => 'above', 'exclude' => 0),
                'teaser' => array('format' => 'default', 'exclude' => 0),
                'full'   => array('format' => 'default', 'exclude' => 0),
                4 => array('format' => 'default', 'exclude' => 0),
                2 => array('format' => 'default', 'exclude' => 0),
                3 => array('format' => 'default', 'exclude' => 0),
                ),
            ),            
        );
        $content['extra']  = array('title' => '-2', 'body_field' => '-1', 'language' => '2', 'menu' => '3', 'attachments' => '30');