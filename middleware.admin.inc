<?php

/**
 * @file
 * Middleware administration page callbacks.
 */

/**
 * Middleware server overview page
 * Path: /content/middleware/overview
 *
 * @return unknown
 */
function middleware_admin_overview() {
    // Overview table header
    $header = array(t('Middleware server statisitcs and current state'), ' ');

    // Collect statistics on posted data
    // 
    // Data posted total count
    $result = db_fetch_array(db_query('SELECT count(*) AS mw_data_count FROM {mw_data}'));
    $rows[] = array(t('Total data posted count'), $result['mw_data_count']); 
    
    // Data posted and unpublished 
    $result = db_fetch_array(db_query('SELECT count(*) AS mw_unp_data_count FROM {mw_data} WHERE status = %d', MW_PROCESS_NEW));
    $rows[] = array(t('Unpublished data'), $result['mw_unp_data_count']); 

    // Data posted and published
    $result = db_fetch_array(db_query('SELECT count(*) AS mw_p_data_count FROM {mw_data} WHERE status = %d', MW_PROCESS_DONE));
    $rows[] = array(t('Published data'), $result['mw_p_data_count']); 

    // Data posted and unpublished due to misc errors
    $result = db_fetch_array(db_query('SELECT count(*) AS mw_err_data_count FROM {mw_data} WHERE status = %d', MW_PROCESS_ERROR));
    $rows[] = array(t('Publication errors'), $result['mw_err_data_count']); 

    // Settings: 
    $rows[] = array(t('QUEUE_LIMIT_PER_CALL'), QUEUE_LIMIT_PER_CALL); 
    
    $output = theme('table', $header, $rows);
    return $output;
}

function middleware_admin_security() {
    return drupal_get_form('middleware_settings_form');
}

function middleware_admin_instances() {
	$query = "SELECT * FROM {node} n, {mw_instances} i WHERE n.nid = i.nid";
	$result = db_query($query);
	
	$header = array(t('Nid'), t('Title'), t('URL'), t('Type'), t('Operations'));
	$rows   = array();
	while ($fetched_row = db_fetch_array($result)) {
		$row = array();
        $row[] = $fetched_row['nid'];
		$row[] = $fetched_row['title'];
        $row[] = $fetched_row['iurl'];
        $row[] = $fetched_row['connector_type'];
        $row[] = l(t('Edit'), 'node/' . $fetched_row['nid'] . '/edit', array('query' => array('destination' => 'admin/content/middleware/instances')));
        $rows[] = $row;
	}
	
    $elements = array(
        'table' => array('#value' => theme('table', $header, $rows), '#weight' => 0),
        'links' => array('#value' => l(t('Add new instance'), 'node/add/mw-instance', array('query' => array('destination' => 'admin/content/middleware/nstances'))), '#weight' => 1) 
    );
    
    return drupal_render($elements);	
}

function middleware_admin_routes() {
    $query = "SELECT r.nid, n3.title AS route_t, n.title AS source_t, n2.title AS dest_t, r.data_type FROM {mw_routes} r JOIN {node} n ON r.source = n.nid JOIN {node} n2 ON r.destination = n2.nid JOIN {node} n3 ON r.nid = n3.nid";
    $result = db_query($query);
    
    $header = array(t('Nid'), t('Title'), t('Source'), t('Destination'), t('Type'), t('Operations'));
    $rows   = array();
    while ($fetched_row = db_fetch_array($result)) {
        $row = array();
        $row[] = $fetched_row['nid'];
        $row[] = $fetched_row['route_t'];        
        $row[] = $fetched_row['source_t'];
        $row[] = $fetched_row['dest_t'];
        $row[] = $fetched_row['data_type'];
        $row[] = l(t('Edit'), 'node/' . $fetched_row['nid'] . '/edit', array('query' => array('destination' => 'admin/content/middleware/routes')));
        $rows[] = $row;
    }
    
    $elements = array(
        'table' => array('#value' => theme('table', $header, $rows), '#weight' => 0),
        'links' => array('#value' => l(t('Add new route'), 'node/add/mw-route', array('query' => array('destination' => 'admin/content/middleware/routes'))), '#weight' => 1) 
    );
    
    return drupal_render($elements);
}

/*
 * Try to retrieve key consistency status, time-expensive operation!
 */
function middleware_admin_mapping() {	
	// Get all sources and destinations
	$map_groups = array();
	$result = db_query('SELECT data_type, GROUP_CONCAT(DISTINCT destination) as destinations, GROUP_CONCAT(DISTINCT source) as sources FROM {mw_routes} GROUP BY data_type');		
    while ($row = db_fetch_array($result)) {
    	// Combine sources and instances into one array
    	$instances = array_unique(array_merge(explode(',', $row['sources']), explode(',', $row['destinations'])));
    	$map_groups[$row['data_type']] = $instances;    	
    }
    
    // Check for consistency
    foreach ($map_groups as $map_group => $instances) {
    	switch ($map_group) {
    		case 'user' : 
    			$key_fields = array('uid');
    			break;
    		default: 
    			$key_fields = array();
    	}
    	
    	$where_sql = '(instance = ' . implode(' OR instance = ', $instances) . ')';
    	$issues = array();    	
    	foreach ($key_fields as $key_field) {
    		$key = $map_group . '_' . $key_field;    		
    		$query = "SELECT COUNT(value) AS n, mw_id FROM {mw_keys} WHERE key_name LIKE '%s' AND " . $where_sql . ' GROUP BY mw_id HAVING n < ' . count($instances);
    		$result = db_query($query, db_escape_string($key));
    		$totals[$key] = array();
    		while ($row = db_fetch_array($result)) {
    			$totals[$key][] = $row;
    		}
    		if (count($totals[$key]) > 0) {
    			$issues[] = t('Total %num maps are incomplete for %key key', array('%num' => count($totals[$key]), '%key' => $key));
    			//drupal_set_message(t('Keys incomplte row dump: ') . var_export($totals, TRUE));
    		} 
    	}
    }

    // Check for duplicated keys
    $query = "SELECT COUNT(*) AS dup_count, key_name FROM (SELECT COUNT(mw_id) AS n, key_name FROM {mw_keys} GROUP BY instance, key_name, value HAVING n > 1) AS t1 GROUP BY key_name";
    $result = db_query($query);
    while ($row = db_fetch_array($result)) {
        if ($row['dup_count'] > 0) {
            $issues[] = t('Total %num keys are duplicated for %keyname key', array('%num' => $row['dup_count'], '%keyname' => $row['key_name']));
        }    	
    }
    
    $status_text = (count($issues) > 0) ? implode('<br>', $issues) : t('OK');    
	
	$elements = array(
	   'status' => array(
	       '#value'  => t('Current status:<br>') . $status_text,
	       '#weight' => 0, 
        ),
        'fix_form' => array(
           '#value'  => drupal_get_form('middleware_fix_mapping_form'),
           '#weight' => 1,
        ),
        'import_form' => array(
           '#value'  => drupal_get_form('middleware_import_mapping_form'),
           '#weight' => 2,
        ),
    );
	
    return drupal_render($elements);
}

function middleware_settings_form($form_state) {
    $form = array();
    $token = isset($form_state['values']['mw_token']) ? $form_state['values']['mw_token'] : variable_get('mw_server_token', '0'); 
        
    $form['mw_token'] = array(
       '#type'          => 'textfield',
       '#title'         => t('Server identification token'),
       '#required'      => TRUE,
       '#default_value' => $token,
       '#weight'        => 0,
       '#suffix' => '<div id="mw_token_wrapper"></div>',
    );
    $form['submit'] = array(
       '#type'   => 'submit',
       '#value'  => t('Save configuration'),
       '#weight' => 2
    );
    $form['regenerate'] = array(
        '#type'   => 'submit',
        '#submit' => array('middleware_generate_token_submit'),
        '#value'  => t('Generate new token'),
        '#weight' => 1,
        '#ahah'   => array(
            'path'     => 'middleware/settings/generate_token',
            'wrapper'  => 'mw_token_wrapper',
            'method'   => 'replace',
            'progress' => '',          
       )
    );
    
    return $form;
}

function middleware_generate_token_js() {
    // Generate random MD5 hash string
	$token_string = ''; 
    for ($i = 0; $i < 16; $i++) {
        $token_string .= chr(mt_rand(0, 255));               
    }
	$token = t('Randomly generated token string: ') . md5($token_string);
	print drupal_to_js(array('status' => TRUE, 'data' => $token));
}

function middleware_settings_form_validate($form, &$form_state) {
    // Check if valid md5 string supplied
    if ($form_state['clicked_button']['#id'] == 'edit-submit') {
        if (strlen($form_state['values']['mw_token']) != 32) {
            form_set_error('mw_token' , t('Middleware server token must be 32 characters length.'));            
        }
        elseif (!ctype_alnum($form_state['values']['mw_token'])) {
            form_set_error('mw_token' , t('Middleware server token must be valid MD5 hash string'));                    
        }
    }
}

function middleware_settings_form_submit($form, &$form_state) {
    variable_set('mw_server_token', $form_state['values']['mw_token']);
}

/**
 * Import foreign keys mapping from CSV file
 *
 * @param array $form_state
 */
function middleware_import_mapping_form($form_state) {
	$form = array();
	$form['import'] = array(
	   '#type'        => 'fieldset',
       '#attributes'  => array('enctype' => "multipart/form-data"),
	   '#title'       => t('Import mapping'),
	   '#collapsible' => TRUE,
	   '#collapsed'   => TRUE,   
	);
	$form['import'] += array(
       'instance_one' => array(
           '#type'    => 'select',
           '#title'   => t('First instance'),
           '#description' => t('Instance for the first column'),
           '#default_value' => $form_state['values']['instance_one'],
           '#options' => _get_available_instances(), 
           '#required'   => TRUE, 
       ),
       'instance_two' => array(
           '#type'    => 'select',
           '#title'   => t('Second instance'),       
           '#description' => t('Instance for the second column'),     
           '#default_value' => $form_state['values']['instance_two'],
           '#options' => _get_available_instances(),
           '#required'   => TRUE,        
       ),
       'data_type' => array(
           '#type'    => 'select',
           '#title'   => t('Data type'),     
           '#description' => t('Select data type for this mapping'),     
           '#options' => _get_available_data_types(),
           '#required'   => TRUE, 
       ),
       'field_name' => array(
           '#type'    => 'textfield',
           '#title'   => t('Field name'),     
           '#description' => t('A field from the previously selected data type which this mapping table applies against'),
           '#size'      => 38,
           '#maxlength' => 32,
           '#required'   => TRUE, 
       ),
       'mapping_file' => array(
           '#type'  => 'file',
           '#title' => t('Mapping file'),
           '#description' => t('Must be comma-separeted CSV file with two columns. Values start from the first row (no header).'),
       ),
       'submit' => array(
           '#type'  => 'submit',
           '#value' => t('Import'),
       ),
    );
    
    return $form;
}

function middleware_import_mapping_form_validate($form, &$form_state) {
 // TODO: Validate data
 
    // Check if instances are not the same
    if ($form_state['values']['instance_one'] == $form_state['values']['instance_two']) {
        form_set_error('instance_one', t('Instances must not be identical.'));
    }
}

function middleware_import_mapping_form_submit($form, &$form_state) {
    $inst1 = $form_state['values']['instance_one'];
    $inst2 = $form_state['values']['instance_two'];
    $type  = $form_state['values']['data_type'];
    $field = $form_state['values']['field_name'];
    
    $validators = array('file_validate_extensions' => array('csv')); 
    if ($file = file_save_upload('mapping_file', $validators)) {
        $handle = fopen($file->filepath, 'r');
        while ($row = fgetcsv($handle, 1000, ',')) {
            // Add mapping instance1 <-> middleware and instance2 <-> middleware
            $mw_key = _middleware_save_fq($type, $field, $inst1, $row[0]);
            _middleware_add_fq_map($type, $field, $inst2, $mw_key, $row[1]);
        }
    }
    else {
        form_set_error('mapping_file', 'Unable to open CSV file.');
    }
    drupal_set_message('Data was successfully imported.');
}

function middleware_fix_mapping_form($form_state) {
    $form = array();
    $form['fix'] = array(
       '#type'        => 'fieldset',
       '#attributes'  => array('enctype' => "multipart/form-data"),
       '#title'       => t('Fix mapping'),
       '#collapsible' => TRUE,
       '#collapsed'   => TRUE,   
    );  
    $form['fix'] += array(
       'submit' => array(
           '#type'  => 'submit',
           '#value' => t('Fix problem keys'),
       ),
    );
    
    return $form;
}

function middleware_fix_mapping_form_submit($form, &$form_state) {
    $server_token = variable_get('mw_server_token', NULL);
    $limit = defined('REPAIR_QUEUE_LIMIT_PER_CALL') ? REPAIR_QUEUE_LIMIT_PER_CALL : 10;
    
	
    // Known data types available for replication
    $data_types = array(
        'user' => array(
            'keys' => array(
                'uid'  => array('enabled' => TRUE,  'transparent' => FALSE),
                'name' => array('enabled' => FALSE, 'transparent' => TRUE),
            ),
        ),
    );    
    
    foreach ($data_types as $data_type => $description) {
    	// Select transparent keys for key mapping
    	$tkeys = array();
    	foreach ($description['keys'] as $key => $parameters) {
    		if ($parameters['transparent']) {
    			$tkeys[$key] = TRUE;
    		}
    	} 
        foreach ($description['keys'] as $key => $parameters) {
        	if ($parameters['enabled'] == FALSE) {
        		continue;
        	}
            $key_name = $data_type . '_' . $key; 

            // Retrieve a sources list from the routes which this data type is assinged to
            $instances = array();
            $query = "SELECT source FROM {mw_routes} WHERE data_type LIKE '%s' UNION SELECT destination FROM {mw_routes} WHERE data_type LIKE '%s'"; 
            $result = db_query($query, $data_type, $data_type);
            while ($row = db_fetch_array($result)) {
            	array_push($instances, $row['source']);
            }            
            
            $mw_ids = array();

            // Find incomplete mappings
            $query = "SELECT COUNT(value) AS n, mw_id FROM {mw_keys} WHERE key_name LIKE '%s' AND " .
                     "instance IN (" . implode(',', $instances) . ") GROUP BY mw_id " .
                     "HAVING n < (SELECT COUNT(*) FROM (SELECT source FROM {mw_routes} WHERE data_type LIKE '%s' UNION SELECT destination FROM {mw_routes} WHERE data_type LIKE '%s') AS t1) LIMIT " . $limit;
            
            $result = db_query($query, $key_name, $data_type, $data_type);
            while ($row = db_fetch_array($result)) {
                array_push($mw_ids, $row['mw_id']);
            }
            
            // Duplicates
            $duplicate_mw_ids = array();
            $query = "SELECT mw_id, GROUP_CONCAT(mw_id) AS ids, COUNT(mw_id) AS n FROM {mw_keys} WHERE key_name LIKE '%s' GROUP BY instance, key_name, value HAVING n > 1 LIMIT " . $limit;
            $result = db_query($query, $key_name);
            while ($row = db_fetch_array($result)) {
                array_push($mw_ids, $row['mw_id']);
                $duplcates = array_diff(explode(',', $row['ids']), array($row['mw_id'])); 
                $duplicate_mw_ids[$row['mw_id']] = $duplcates;
                
                // Remove duplicates from processing queue
                $mw_ids = array_diff($mw_ids, $duplcates);
            }            
            
            $mw_ids = array_unique($mw_ids);

            $count         = 0;
            $deleted_count = 0;
            $maps_array    = array();
            $values        = array();
            
            // Step one. Retrieve any available instance/key pair for each incomplete mapping
            foreach($mw_ids as $mw_id) {
                // Retrieve key associated with mw_id
                $row = db_fetch_array(db_query("SELECT * FROM {mw_keys} WHERE mw_id = '%d'", $mw_id));                                        
                list($type, $field) = explode('_', $row['key_name']);
            	$maps_array[$mw_id] = array(
                    'type'   => $type,
                    'field'  => $field,
                    'inst_values' => array(
                        $row['instance'] => $row['value'], 
                    ),
            	    'transparent_values' => array(), 
            	);
            	$values[$type][$field][$row['instance']][] = array(
            	   'value' => $row['value'],
            	);
            }
            
            foreach ($values as $type_name => $fields) {
            	foreach ($fields as $field_name => $minstance) {
                    // Step two. Try to retrieve transparent key for each incomplete mapping
            		foreach ($minstance as $instance_id => $values) {
                        $inst_data = _get_instance($instance_id);
                        
                        $keys = array();
                        foreach ($values as $i => $pair) {
                        	array_push($keys, $pair['value']);
                        }                        
                        
            			$rpc_res = xmlrpc($inst_data['iurl'], 'mw.mass_getdata_filtered', $server_token, $type_name, $field_name, base64_encode(serialize($keys)), $tkeys);            			
                        if ($rpc_res and isset($rpc_res['status']) and ($rpc_res['status'] == MW_OK)) {
                            if (isset($rpc_res['data'])) {
                            	$rpc_res['data'] = unserialize(base64_decode($rpc_res['data']));
                            	if (is_array($rpc_res['data'])) {
	                            	// Append result to mapping array
	                            	foreach ($maps_array as $mw_id => &$map) {
	                            		if (isset($map['inst_values'][$instance_id]) and isset($rpc_res['data'][$map['inst_values'][$instance_id]])) {
	                                        $map['transparent_values'] += $rpc_res['data'][$map['inst_values'][$instance_id]];
	                            		} 
	                            	}
	                            	unset($map); // Avoid reference weird behavior
                            	}                             	
                            }                        	
                        }
            		}
            		
            		// Step three. Update map for each instance using transparent keys from the step two
                    foreach ($instances as $instance_id) {
                        $keys = array();
                        foreach ($maps_array as $mw_id => $map_data) {
                            if (!isset($map_data['inst_values'][$instance_id])) {
                                if (count($map_data['transparent_values']) > 0) {
                                	foreach ($map_data['transparent_values'] as $k => $v) {
                                        $keys[$k][] = $v;
                                	}
                                }
                            }                   
                        }
                        
                        foreach ($keys as $k_name => $k) {
	                        if (count($k) > 0) {
	                            $inst_data = _get_instance($instance_id);
	                            $parameters = unserialize($inst_data['parameters']);

	                            
	                            if (!$parameters['overwrite_map_source']) {
	                                $rpc_res = xmlrpc($inst_data['iurl'], 'mw.mass_getdata_filtered', $server_token, $type_name, $k_name, base64_encode(serialize($k)), array($field_name => TRUE));
	                                
                                    drupal_set_message(xmlrpc_error_msg());
                                    
	                                if ($rpc_res and ($rpc_res['status'] == MW_OK)) {
                                        $rpc_res['data'] = unserialize(base64_decode($rpc_res['data']));
                                        if (is_array($rpc_res['data'])) {
	                                        //$map[$instance_id] = $rpc_res2['data'][$key];
	                                        foreach ($maps_array as $mw_id => &$map) {                                                                                  
	                                            if (isset($map['transparent_values'][$k_name])) {
	                                                if (isset($rpc_res['data'][$map['transparent_values'][$k_name]])) {
	                                                    $map['inst_values'][$instance_id] = $rpc_res['data'][$map['transparent_values'][$k_name]][$field_name];
	                                                }
	                                            }
	                                        }
	                                        unset($map); // Avoid reference weird behavior
                                        }	                                	                                        
	                                }
	                            }
	                        }
                        }     
                    } // foreach                    
            	}            	
            }            
            
            // Final step: update maps
            foreach ($maps_array as $mw_id => $map_data) {
                if (count($map_data['inst_values']) == count($instances)) {
                    // Remove old keys
                    $query = 'DELETE FROM {mw_keys} WHERE mw_id = %d';
                    db_query($query, $mw_id);
                            
                    if ($duplicate_mw_ids[$mw_id]) {
                        foreach ($duplicate_mw_ids[$mw_id] as $mw_id_dup) {
                            db_query($query, $mw_id_dup);
                        }
                        unset($duplicate_mw_ids[$mw_id]);                           
                    }
  
                    // Save new map
                    $new_mw_id = NULL;
                    foreach ($map_data['inst_values'] as $i => $v) {
                        if (!$new_mw_id) {
                            db_query("INSERT INTO {mw_keys} (key_name, instance, value) VALUES ('%s', %d, '%s')", $key_name, $i, $v);
                            $new_mw_id = (int) db_last_insert_id('mw_keys', 'mw_id');
                        }
                        else {
                            db_query("INSERT INTO {mw_keys} (mw_id, key_name, instance, value) VALUES (%d, '%s', %d, '%s')", $new_mw_id, $key_name, $i, $v);
                        }
                    } // foreach
                } // if ($complete)
            }
            
            // Remove duplicates
            $query = 'DELETE FROM {mw_keys} WHERE mw_id = %d';
            foreach ($duplicate_mw_ids as $mw_id_orig => $mw_id_dups) {
                if (is_array($mw_id_dups)) {
                    foreach($mw_id_dups as $dup_id) {
                        if (db_query($query, $dup_id)) {
                            $deleted_count++;
                        }
                    }
                }
            }            
        }       
    }        	
	
    if ($deleted_count > 0) {
        drupal_set_message(t('Removed %count keys.', array('%count' => $deleted_count)));    	
    }
    drupal_set_message(t('Key mappings were successfuly fixed.'));
}

function middleware_data_form($form_state) {
    $form = middleware_data_filter_form();

    $form['#theme'] = 'middleware_data_filter_form';
    $form['admin']  = middleware_admin_data();
    
    return $form;    
}

function middleware_admin_data() {    
    $filter = middleware_build_data_filter_query();
    if ($filter['where'] != '') {
    	$filter['where'] .= ' AND d.source = n.nid';
    }
    else {
    	$filter['where'] = 'WHERE d.source = n.nid';
    }
    $result = pager_query('SELECT did, title, data_type, action, d.status AS dstatus, note FROM {mw_data} d, {node} n ' . $filter['where'] . ' ORDER BY did DESC', 50, 0, NULL, $filter['args']);

    $form['options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update options'),
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    );
    $options = array('delete' => 'delete', 'reset' => 'reset');
    $form['options']['operation'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => 'reset',
    );
    $form['options']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
      '#submit' => array('middleware_admin_data_submit'),
    );
    
    while ($data = db_fetch_object($result)) {
        $data_array[$data->did] = '';
        $form['source'][$data->did]    = array('#value' => $data->title);
        $form['data_type'][$data->did] = array('#value' => $data->data_type);
        $form['action'][$data->did]    = array('#value' => $data->action);
        $form['status'][$data->did]    = array('#value' => $data->dstatus);
        $form['note'][$data->did]      = array('#value' => $data->note);
        $form['actions'][$data->did]   = array('#value' => $data->did);
    }
  $form['data_array'] = array('#type' => 'checkboxes', '#options' => $data_array);
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['#theme'] = 'middleware_admin_data';
  return $form;    
}

/**
 * Theme middleware data administration overview. (Copypaste from node.admin.inc)
 *
 * @ingroup themeable
 */
function theme_middleware_admin_data($form) {
    // If there are rows in this form, then $form['source'] contains a list of
    // the source form elements.
    $has_data = isset($form['source']) && is_array($form['source']);
    $select_header = $has_data ? theme('table_select_header_cell') : '';
    $header = array($select_header, t('Source'), t('Type'), t('Action'), t('Status'), t('Note'), t('Actions'));
    
    $output = '';
    $output .= drupal_render($form['options']);
    if ($has_data) {
        foreach (element_children($form['source']) as $key) {
            $row = array();
            $row[] = drupal_render($form['data_array'][$key]);
            $row[] = drupal_render($form['source'][$key]);
            $row[] = drupal_render($form['data_type'][$key]);
            $row[] = drupal_render($form['action'][$key]);
            $status_string = array(
                MW_PROCESS_NEW     => t('New'),
                MW_PROCESS_DONE    => t('Success'),
                MW_PROCESS_ERROR   => t('Error'),
                MW_PROCESS_TIMEOUT => t('Timeout'),
            );
            $row[] = $status_string[$form['status'][$key]['#value']];
            $row[] = drupal_render($form['note'][$key]);
            $row[] = l(t('View'), 'admin/content/middleware/data/view/' . $form['actions'][$key]['#value']);
            $rows[] = $row;
        }
    }
    else {
        $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '7'));
    }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  return $output;
}

/**
 * Process middleware_admin_data form submissions.
 * 
 * Execute the chosen option on the selected middleware data chunks.
 */
function middleware_admin_data_submit($form, &$form_state) {
    // Filter out unchecked nodes
    $data_array = array_filter($form_state['values']['data_array']);
    
    switch ($form_state['values']['operation']) {
        case 'delete':
            foreach ($data_array as $did) {
                db_query('DELETE FROM {mw_data} WHERE did = %d', $did);
            }
            break;
        case 'reset':
            foreach ($data_array as $did) {
                db_query('UPDATE {mw_data} SET status = %d WHERE did = %d', MW_PROCESS_NEW, $did);
            }
            break;
        default:
    }
    $form_state['rebuild'] = TRUE;
}


/**
 * TODO: Build query for middleware data administration filters based on session.
 */
function middleware_build_data_filter_query() {
    $filters = middleware_data_filters();

    // Build query
    $where = $args = array();
    $join = '';
    foreach ($_SESSION['middleware_data_overview_filter'] as $index => $filter) {
    	list($key, $value) = $filter;
        switch ($key) {
            case 'status':
                $where[] = 'd.status = %d';
                break;
            case 'data_type':
                $where[] = "d.data_type = '%s'";
                break;
            case 'action':
                $where[] = "d.action = '%s'";
            	break;
            case 'source':
                $where[] = "d.source = '%s'";
                break;
        }
        $args[] = $value;
    }
    $where = count($where) ? 'WHERE '. implode(' AND ', $where) : '';

    return array('where' => $where, 'join' => $join, 'args' => $args);
}


/**
 * Return form for middleware data administration filters.
 */
function middleware_data_filter_form() {
    $session = &$_SESSION['middleware_data_overview_filter'];
    $session = is_array($session) ? $session : array();
    $filters = middleware_data_filters();

    $i = 0;
    $form['filters'] = array(
      '#type' => 'fieldset',
      '#title' => t('Show only data where'),
      '#theme' => 'middleware_data_filters',
    );
    $form['#submit'][] = 'middleware_data_filter_form_submit';
    foreach ($session as $filter) {
        list($type, $value) = $filter;
        $value = $filters[$type]['options'][$value];
        if ($i++) {
          $form['filters']['current'][] = array('#value' => t('<em>and</em> where <strong>%a</strong> is <strong>%b</strong>', array('%a' => $filters[$type]['title'], '%b' => $value)));
        }
        else {
          $form['filters']['current'][] = array('#value' => t('<strong>%a</strong> is <strong>%b</strong>', array('%a' => $filters[$type]['title'], '%b' => $value)));
        }
        if (in_array($type, array('type', 'language'))) {
          // Remove the option if it is already being filtered on.
          unset($filters[$type]);
        }
    }
    foreach ($filters as $key => $filter) {
        $names[$key] = $filter['title'];
        $form['filters']['status'][$key] = array('#type' => 'select', '#options' => $filter['options']);
    }

    $form['filters']['filter'] = array('#type' => 'radios', '#options' => $names, '#default_value' => 'status');
    $form['filters']['buttons']['submit'] = array('#type' => 'submit', '#value' => (count($session) ? t('Refine') : t('Filter')));
    if (count($session)) {
      $form['filters']['buttons']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
      $form['filters']['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
    }
    
    drupal_add_js('misc/form.js', 'core');
    
    return $form;
}

/**
 * Process result from middleware data administration filter form.
 */
function middleware_data_filter_form_submit($form, &$form_state) {
  $filters = middleware_data_filters();
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      if (isset($form_state['values']['filter'])) {
        $filter = $form_state['values']['filter'];

        // Flatten the options array to accommodate hierarchical/nested options.
        $flat_options = form_options_flatten($filters[$filter]['options']);

        if (isset($flat_options[$form_state['values'][$filter]])) {
          $_SESSION['middleware_data_overview_filter'][] = array($filter, $form_state['values'][$filter]);
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['middleware_data_overview_filter']);
      break;
    case t('Reset'):
      $_SESSION['middleware_data_overview_filter'] = array();
      break;
  }
}

/**
 * List middleware data administration filters that can be applied.
 */
function middleware_data_filters() {
    // Regular filters
    $filters['status'] = array(
        'title' => t('status'),
        'options' => array(
            MW_PROCESS_NEW     => t('New'),
            MW_PROCESS_DONE    => t('Success'),
            MW_PROCESS_ERROR   => t('Error'),
            MW_PROCESS_TIMEOUT => t('Timeout'),
        ),
    );

    // Data type
    $filters['data_type'] = array('title' => t('type'), 'options' => _get_available_data_types());

    // Source/destination instance
    $filters['action'] = array(
        'title' => t('action'),
        'options' => array(
            'create' => 'create', 
            'update' => 'update', 
            'remove' => 'remove',
        )
    );
    $filters['source'] = array('title' => t('source'), 'options' => _get_available_instances());
    
    return $filters;
}

/**
 * Theme middleware data administration filter selector.
 *
 * @ingroup themeable
 */
function theme_middleware_data_filters($form) {
  $output = '';
  $output .= '<ul class="clear-block">';
  if (!empty($form['current'])) {
    foreach (element_children($form['current']) as $key) {
      $output .= '<li>'. drupal_render($form['current'][$key]) .'</li>';
    }
  }

  $output .= '<li><dl class="multiselect">'. (!empty($form['current']) ? '<dt><em>'. t('and') .'</em> '. t('where') .'</dt>' : '') .'<dd class="a">';
  foreach (element_children($form['filter']) as $key) {
    $output .= drupal_render($form['filter'][$key]);
  }
  $output .= '</dd>';

  $output .= '<dt>'. t('is') .'</dt><dd class="b">';

  foreach (element_children($form['status']) as $key) {
    $output .= drupal_render($form['status'][$key]);
  }
  $output .= '</dd>';

  $output .= '</dl>';
  $output .= '<div class="container-inline" id="node-admin-buttons">'. drupal_render($form['buttons']) .'</div>';
  $output .= '</li></ul>';

  return $output;
}

function middleware_data_view($did) {
	$data = db_fetch_object(db_query('SELECT * FROM {mw_data} WHERE did = %d', $did));
	return var_export(unserialize($data->data), TRUE);
}