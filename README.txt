
DESCRIPTION
-----------
Drupal Middleware is used to establish XML-RPC based data exchange 
between drupal and external systems inculding another drupal 
instances. Data can be replicated in many directions using 
different routes with different rules applied. Creation, updates 
and removals are separated. Pluggable connectors are take care of 
system-specific implementations.

INSTALLATION
------------
Download and enable module. 

USAGE
-----
Any standalone system within multi-system landscape is called "Instance". 
Instance can accept and generate data of different types. Module adds
special content type "MW Instance" to identify such a system.
The second step is to establish a route. Route is compound of source 
and destination instances and a content type apllied to this route. 

Drupal sites use Middleware client module to post their data on 
middleware. External systems must use dmwclient class. Read documentation
on client module to get additional information for client set-up.

REQUIREMENTS
------------
No additional modules are reqiured

FAQ
---

LIMITATIONS
-----------
Drupal Middleware is under heavy development. It's still possible for
architecture design to be cardinally changed so back compatibility for
future releases is not guaranteed.

CREDITS
-------
Design and implementation by Mikhail Khorpyakov